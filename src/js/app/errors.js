function newClaim(){
  var departure          = document.querySelector('#departures');
  var arrival           = document.querySelector('#arrivals');
  var correspondance       = document.querySelector('.correspondance');
  var no_correspondance    = document.querySelector('.no_correspondance');
  var correspondance_city      = document.querySelector('.correspondance_city')
  var errors               = [];
  var succes               = [];
  var errors_block         = document.querySelector('.errors');
  if (departure.value == "") {
    errors.push("Entrez votre lieux de départ");
  }
  if (arrival.value == "") {
    errors.push("Entrez votre lieux d'arrivée");
  }
  if (correspondance.checked != true && no_correspondance.checked != true) {
    errors.push("Avez-vous eu des correspondances");
  }
  if (correspondance.value != "oui") {
    errors.push("Avez-vous eu des correspondances");
  }
  if (no_correspondance.value != "non") {
    errors.push("Avez-vous eu des correspondances");
  }
  if (correspondance.checked == true && correspondance_city.value == "" ){
    errors.push("Entrez vos/votre correspondances");
  }
  if (errors.length != 0) {
    errors_block.innerText = errors;
    return false
  }
  else {
    return true
  }
}

function itenary(){
  var retarded     = document.querySelector('.retarded');
  var canceled     = document.querySelector('.canceled');
  var refused      = document.querySelector('.refused');
  var errors_block = document.querySelector('.errors');
  var less         = document.querySelector('.less');
  var more         = document.querySelector('.more');
  var never        = document.querySelector('.never');
  var reason      = document.querySelector('.reason');
  var errors       = [];
  var succes       = [];
  if (retarded.checked != true && canceled.checked != true && refused.checked != true) {
    errors.push('Sélectionner ce qui est arrivé à votre vol');
  }
  if (retarded.value != "retardé") {
    errors.push('Erreur sur le retard');
  }
  if (canceled.value != "annulé") {
    errors.push("Erreur sur l'annulation");
  }
  if (refused.value != "refusé") {
    errors.push('Erreur sur le refus');
  }
  if (less.checked != true && more.checked != true && never.checked != true) {
    errors.push('Sélectionner le retard de votre vol');
  }
  if (less.value != "-3h") {
    errors.push('Erreur sur la durée du retard');
  }
  if (more.value != "+3h") {
    errors.push('Erreur sur la durée du retard');
  }
  if (never.value != "jamais") {
    errors.push('Erreur sur la durée du retard');
  }
  if (reason.value == '') {
    errors.push('Entrez la raison du retard évoquée par la compagnie');
  }
  if (reason.value != 'Problème technique' && reason.value != 'Mauvaise météo' && reason.value != "Influence d'autres vols" && reason.value != "Problèmes à l'aéroport" && reason.value != "Grève" && reason.value != "Aucune raison donnée" && reason.value != "Je ne me souviens pas") {
    errors.push('Mauvaise raison');
  }
  if (errors.length > 0) {
    errors_block.innerText = errors;
    return false
  }
  else {
    return true
  }
}

function fly_number(){
  var company       = document.querySelector('.company');
  var number_of_fly = document.querySelector('.number_of_fly');
  var date          = document.querySelector('.date');
  var errors_block = document.querySelector('.errors');
  var errors        = [];
  var succes        = [];
  if (company.value == '') {
    errors.push('Entrez le nom de la compagnie aerienne');
  }
  if (number_of_fly.value == '') {
    errors.push('Entrez le numéro de vol');
  }
  if (number_of_fly.length > 6) {
    errors.push('Entrez un bon numéro de vol');
  }
  if (date.value == "") {
    errors.push('Entrez votre la date du vol');
  }
  if (errors.length > 0) {
    errors_block.innerText = errors;
    return false
  }
  else {
    return true
  }
}

function signUp(){
  var first_name = document.querySelector('.first_name');
  var last_name  = document.querySelector('.last_name');
  var email      = document.querySelector('.email');
  var password   = document.querySelector('.password');
  var password2  = document.querySelector('.password2');
  var birth_date = document.querySelector('.birth_date');
  var errors_block = document.querySelector('.errors');
  var errors     = [];
  var succes     = [];

  if (first_name.value == '') {
    errors.push('Entrez votre nom');
  }
  if (last_name.value == '') {
    errors.push('Entrez votre prénom');
  }
  if (email.value == '') {
    errors.push('Entrez votre email');
  }
  if (password.value == '') {
    errors.push('Entrez votre mot de passe');
  }
  if (password2.value == '') {
    errors.push('Entrez votre mot de passe');
  }
  if (password.value != password2.value) {
    errors.push('Les deux mots de passe ne sont pas identiques');
  }
  if (birth_date.value == '') {
    errors.push('Entrez votre date de naissance');
  }
  if (errors.length > 0) {
    errors_block.innerText = errors;
    console.log(errors);
    return false
  }
  else {
    console.log(errors);
    return true
  }
}
