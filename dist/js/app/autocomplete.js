$( function() {
  var availableTags = [
    "Paris -- CDG",
    "Tel-Aviv -- BEN GURION",
    "Madrid -- ",
    "Eilat -- OVDA",
    "Bangkok -- ",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
  ];

  $( "#departures" ).autocomplete({
    source: availableTags
  });

  $( "#arrivals" ).autocomplete({
    source: availableTags
  });

} );
