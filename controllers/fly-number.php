<?php

	$title = 'Réclamation';
	$class = 'claim';

	if (!empty($_POST)) {
		$_SESSION['append'] = strip_tags(trim($_POST['append']));
		$_SESSION['retard']	= strip_tags(trim($_POST['retard']));
		$_SESSION['reason']	= strip_tags(trim($_POST['reasons']));

		$departure           = $_SESSION['departure'];
		$arrival             = $_SESSION['arrival'];
		$correspondance      = $_SESSION['correspondance'];
		$correspondance_city = $_SESSION['correspondance_city'];
		$append              = $_SESSION['append'];
		$retard              = $_SESSION['retard'];
		$reason              = $_SESSION['reason'];
	}
