<?php

	$title = 'Réclamation';
	$class = 'claim';
	$departure           = '';
	$arrival             = '';
	$correspondance      = '';
	$correspondance_city = '';
	$success             = array();
	$errors              = array();

	if (!empty($_POST)) {
		$departure           = strip_tags(trim($_POST['departure']));
		$arrival             = strip_tags(trim($_POST['arrival']));
	}
