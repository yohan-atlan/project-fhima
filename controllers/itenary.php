<?php
	$title = 'Réclamation';
	$class = 'claim';

	// $departure           = $_POST['departure'];
	// $arrival             = $_POST['arrival'];
	// $correspondance      = $_POST['correspondance'];
	// $correspondance_city = $_POST['correspondance_city'];



	if (!empty($_POST)) {
		$_SESSION['departure']           = strip_tags(trim($_POST['departure']));
		$_SESSION['arrival']             = strip_tags(trim($_POST['arrival']));
		$_SESSION['correspondance']      = strip_tags(trim($_POST['correspondance']));
		$_SESSION['correspondance_city'] = strip_tags(trim($_POST['correspondance_city']));

		$departure           = $_SESSION['departure'];
		$arrival             = $_SESSION['arrival'];
		$correspondance      = $_SESSION['correspondance'];
		$correspondance_city = $_SESSION['correspondance_city'];
	}
