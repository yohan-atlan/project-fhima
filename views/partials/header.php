<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?= URL ?>dist/libs/bootstrap/bootstrap.min.js"></script>
	<script src="<?= URL ?>dist/js/app/errors.js"></script>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?= URL ?>dist/libs/fullpage/jquery.fullPage.css" />
	<link type="text/css" rel="stylesheet" href="<?= URL ?>dist/libs/bootstrap/bootstrap.min.css"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?= URL ?>dist/libs/autocomplete/easy-autocomplete.min.css"  media="screen,projection"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" href="<?= URL ?>dist/css/main.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" src="<?= URL ?>dist/libs/fullpage/jquery.fullPage.js"></script>
</head>
<body class="page-<?= $class ?>">
	<header>
		<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="<?= URL ?>">Air indemnisation</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?= URL ?>">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?= URL ?>about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?= URL ?>contact">Contact</a>
                    </li>
										<li>
                        <a class="page-scroll" href="<?= URL ?>signUp">Inscription</a>
                    </li>
										<!-- user connecte -->
										<?php if (isset($_SESSION['user'])): ?>
		       						<li>
												<a href="<?= URL ?>profile">Mon compte</a>
											</li>
											<li>
												<a href="<?= URL ?>controllers/disconnect.php">se deconnecter</a>
											</li>
							      <?php endif ?>

										<!-- user déconnecte -->
										<?php if (!isset($_SESSION['user'])): ?>
											<li>
													<a class="page-scroll" href="<?= URL ?>logIn">LogIn</a>
											</li>
										<?php endif ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

	</header>
