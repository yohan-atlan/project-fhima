
  <footer class="footer col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="container">
      <div>
        <h1>Air indemnisation</h1>
      </div>
      <p class="lead">Etes-vous prêt à demander votre indemnisation ?</p>

      <ul>
        <li>
            <a class="page-scroll" href="<?= URL ?>mentions-legales">Mentions légales</a>
        </li>
      </ul>
    </div>
  </footer>
  <script type="text/javascript" src="<?= URL ?>/dist/js/app/script.js"></script>
</body>
</html>
