<div class="errors"></div>
<div class="new col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="new__form col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-5 col-md-offset-5 col-lg-offset-5">
    <h3>Quel était l'itinéraire de votre voyage ?</h3>
    <form class="" action="itenary"method="post">
      <div class="form-group">
        <input type="texte" name="departure" id="departures" class="form-control" id="departure" placeholder="Paris - CDG" value="<?= $departure ?>">
      </div>
      <div class="form-group">
        <input type="texte" name="arrival" id="arrivals" class="form-control" id="arrival" placeholder="Tel Aviv - BGR" value="<?= $arrival ?>">
      </div>
      <div class="form-group">
        <h3>Aviez-vous des correspondances</h3>
        <label class="radio-inline"><input type="radio" class="correspondance" name="correspondance" value="oui" >OUI</label>
        <label class="radio-inline"><input type="radio" class="no_correspondance" name="correspondance" value="non">NON</label>
      </div>
      <div class="form-group">
        <input type="texte" name="correspondance_city" class="correspondance_city form-control" id="arrival" placeholder="Tel Aviv - BGR" value="<?= $correspondance_city ?>" >
      </div>
      <button type="submit" class="validate btn btn-default" onClick="return newClaim()">Vérifier mon indemnité</button>
    </form>
  </div>
</div>
