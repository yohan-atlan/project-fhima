<div class="errors"></div>
 <div class="fly-number col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="fly-number--etape col-xs-12 col-sm-12 col-lg-12">
    <span>1</span>
    <span>2</span>
    <span>3</span>
    <span>4</span>
  </div>

   <div class="fly-number__recapitulatif col-xs-12 col-sm-3 col-md-3 col-lg-3">
     <h1>Récapitulatif de votre voyage</h1>
     <div class="fly-number__recapitulatif--destination">
       <p><?= $departure ?></p>
       <img src="<?= URL ?>dist/img/form/plane.svg" alt="">
       <p><?= $arrival ?></p>
     </div>
     <p>1 escale</p>
     <p>Raison du retard : <strong><?= $append ?></strong></p>
     <p>Retard à l'arrivée : <strong><?= $retard ?></strong></p>
     <p>Motif de la compagnie : <strong><?= $reason ?></strong></p>
   </div>

   <div class="fly-number__form col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
     <form class="" action="<?= URL ?>signUp" method="post">
       <div class="form-group">
         <label for="company">Compagnie aerienne</label>
         <input type="texte" name="company" class="company form-control" id="departure" placeholder="Air France" value="">
       </div>
       <div class="form-group">
         <label for="number_of_fly">N° de vol</label>
         <input type="texte" name="number_of_fly" class="number_of_fly form-control" id="departure" placeholder="AF1234" value="">
       </div>
       <div class="form-group">
         <label for="date">Date du départ</label>
         <input type="date" name="date" class="date form-control" id="departure" placeholder="Paris - CDG" value="<?= $departure ?>">
       </div>
       <button type="submit" class="validate btn btn-default" onclick="return fly_number()">Continuer</button>
     </form>

   </div>
 </div>
