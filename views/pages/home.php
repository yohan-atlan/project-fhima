<div id="fullpage">
	<section class="home">
		<div class="section">
			<section class="home__intro col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<form class="home__intro--form" action="claim/new" method="post">
					<div class="form-group">
						<input type="texte" name="departure" class="form-control" id="departures"  placeholder="Paris - CDG" value="">
					</div>
					<div class="form-group">
						<input type="texte" name="arrival" class="form-control" id="arrivals" placeholder="Tel Aviv - BGR" value="">
					</div>
					<button type="submit" class="validate btn btn-default">Vérifier mon indemnité</button>
				</form>
			</section>
		</div>
		<div class="section">
			<section class="home__information col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="home__information--title col-xs-12 col-sm-12 col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
					<h2>Pourquoi utiliser AIR HELP ?</h2>
				</div>
				<div class="home__information__items col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<div class="home__information__items--images col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<img class="col-xs-12 col-sm-3 col-md-3 col-lg-3" src="" alt="">
						<img class="col-xs-12 col-sm-3 col-md-3 col-lg-3" src="" alt="">
						<img class="col-xs-12 col-sm-3 col-md-3 col-lg-3" src="" alt="">
						<img class="col-xs-12 col-sm-3 col-md-3 col-lg-3" src="" alt="">
					</div>

					<div class="home__information__items--titles col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h3 class="col-xs-12 col-sm-3 col-md-3 col-lg-3">Gratuit ! Vous n'êtes pas payé ? Nous non plus.</h3>
						<h3 class="col-xs-12 col-sm-3 col-md-3 col-lg-3">Expertise juridique gratuite</h3>
						<h3 class="col-xs-12 col-sm-3 col-md-3 col-lg-3">Indice de satisfaction clients</h3>
						<h3 class="col-xs-12 col-sm-3 col-md-3 col-lg-3">400€ + moyenne des indemnisations</h3>
					</div>
					<div class="home__information__items--descriptions col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">Nous ne sommes payés que si vous touchez une indemnité. C'est aussi simple que cela.</p>
						<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">Vous avez des doutes sur vos droits ? Nous pouvons vous aider. Appelez-nous ou envoyez-nous un message et nous vous éclairerons.</p>
						<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">Nous travaillons chaque jour pour pouvoir répondre au mieux à vos attentes.</p>
						<p class="col-xs-12 col-sm-3 col-md-3 col-lg-3">Le montant moyen de l'indemnisation en France est de 400€ par passager. La prochaine fois, vous y réfléchirez à deux fois avant d'accepter un bon pour un sandwich triangle.</p>
					</div>
				</div>
			</section>
		</div>
		<div class="section">
			<section class="home__information col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="home__information--title col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h2>Mode d'emploi</h2>
				</div>
				<div class="home__information__items col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="home__information__items--item col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<img src="dist/img/home/reclam.svg" alt="">
						<h3>Réclamez votre indemnité.<br>(c'est gratuit !)</h3>
					</div>
					<div class="home__information__items--item col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<img src="dist/img/home/reception-reclam.svg" alt="">
						<h3>AirHelp récupère l'indemnisation qui vous est due par la compagnie aérienne.</h3>
					</div>
					<div class="home__information__items--item col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<img src="dist/img/home/recept-indemn.svg" alt="">
						<h3>Recevez une indemnité pouvant aller jusqu'à 600€.</h3>
					</div>
				</div>
			</section>
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#fullpage').fullpage({
			anchors:['firstPage', 'secondPage', 'thirdPage']
		});
	});
</script>
