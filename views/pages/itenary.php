<div class="errors"></div>
<div class="itenary col-xs-12 col-sm-12 col-md-12 col-lg-12">

  <div class="itenary__recapitulatif col-xs-12 col-sm-3 col-md-3 col-lg-3">
    <h1>Récapitulatif de votre voyage</h1>
    <div class="itenary__recapitulatif--destination">
      <p><?= $departure ?></p>
      <img src="<?= URL ?>dist/img/form/plane.svg" alt="">
      <p><?= $arrival ?></p>
    </div>
    <p>
      <?php
        if ($correspondance == "non") {
          echo "Sans escale";
        }
        else {
          echo "Escale à ".$correspondance_city;
        }
       ?>
    </p>
  </div>

  <div class="itenary__form col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
    <h3>Indiquez-nous le premier vol perturbé</h3>
    <form class="" action="fly-number" method="post">
      <div class="form-group">
        <h3>Qu'est-il arrivé à votre vol ?</h3>
        <label class="radio-inline"><input type="radio" name="append" value="retardé" class="retarded">Vol retardé</label>
        <label class="radio-inline"><input type="radio" name="append" value="annulé" class="canceled">Vol annulé</label>
        <label class="radio-inline"><input type="radio" name="append" value="refusé" class="refused">Embarquement refusé</label>

      </div>
      <div class="form-group">
        <h3>Quel a été votre retard total lors de votre arrivée à <?= $arrival ?></h3>

        <label class="radio-inline"><input type="radio" name="retard" value="-3h" class="less">- de 3H</label>
        <label class="radio-inline"><input type="radio" name="retard" value="+3h" class="more">+ de 3H</label>
        <label class="radio-inline"><input type="radio" name="retard" value="jamais" class="never">Jamais arrivé</label>
      </div>
      <div class="form-group">
        <h3>Quelle raison la compagnie aérienne vous a-t-elle donné ?</h3>
      </div>

      <select class="reason form-control" name="reasons">
        <option value="">Sélectionner une option</option>
        <option value="Problème technique">Problème technique</option>
        <option value="Mauvaise météo">Mauvaise météo</option>
        <option value="Influence d'autres vols">Influence d'autres vols</option>
        <option value="Problèmes à l'aéroport">Problèmes à l'aéroport</option>
        <option value="Grève">Grève</option>
        <option value="Aucune raison donnée">Aucune raison donnée</option>
        <option value="Je ne me souviens pas">Je ne me souviens pas</option>
      </select>
      <button type="submit" class="validate btn btn-default" onClick="return itenary()">Continuer</button>
    </form>
  </div>

</div>
