<?php
  // require 'controllers/contact.php';
?>

<section class="contact col-xs-12 col-sm-12 col-md-12 col-lg-12">

  <div class="contact__informations col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
    <h1>contact us</h1>
    <p>Lorem ipsum dolor sit amet consectetur.</p>
  </div>

  <div class="contact__form col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
    <form class="" method="post">
      <div class="contact__form__left col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="form-group">
            <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Your name">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" name="email" id="InputEmail" aria-describedby="emailHelp" placeholder="Your email">
          </div>
          <div class="form-group">
            <input type="number" class="form-control" name="phone" id="InputNumber" aria-describedby="emailHelp" placeholder="Your phone">
          </div>
      </div>
      <div class="contact__form__right col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
          <textarea class="form-control" name="message" id="comment" placeholder="Your message"></textarea>
        </div>
      </div>
      <div class="contact__form__submit col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
          <button type="submit" name="submit" class="btn btn-primary">Envoyez votre demande</button>
        </div>
      </div>
    </form>
  </div>
</section>
