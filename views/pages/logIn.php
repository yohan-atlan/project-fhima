<div class="logIn col-xs-12 col-sm-12 col-lg-12">
  <div class="logIn__form col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-5">
    <form method="post" action="#" role="login" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h1>Connexion</h1>
      <div class="form-group logIn__form--email">
        <input type="email" name="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
      </div>
      <div class="form-group logIn__form--password">
        <input type="password" name="password" class="form-control" aria-describedby="emailHelp" placeholder="Enter your password">
      </div>
      <div class="form-group logIn__form--submit">
        <button type="submit" name="submit" class="btn btn-lg btn-primary">Connexion</button>
      </div>
    </form>
  </section>
  </div>
</div>
