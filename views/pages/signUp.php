
<div class="signUp col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="signUp__form__connexion col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <form method="post" action="#" role="login" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Connexion</h1>
        <div class="form-group logIn__form--email">
          <input type="email" name="email_login" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
        </div>
        <div class="form-group logIn__form--password">
          <input type="password" name="password_login" class="form-control" aria-describedby="emailHelp" placeholder="Enter your password">
        </div>
        <div class="form-group logIn__form--submit">
          <button type="submit" name="submit_connexion" class="btn btn-lg btn-primary">Connexion</button>
        </div>
      </form>
    </section>
  </div>
  <div class="signUp__form__inscription col-xs-4 col-sm-4 col-md-4 col-lg-4">
    <form class="" action="" method="post">
      <h1>Inscription</h1>
      <div class="form-group">
        <label for="first_name">Firstname</label>
        <input type="text" name="first_name" class="first_name form-control" placeholder="Firstname" value="">
      </div>

      <div class="form-group">
        <label for="last_name">Lastname</label>
        <input type="text" name="last_name" class="last_name form-control" placeholder="Lastname" value="">
      </div>

      <div class="form-group">
        <label for="email">Email</label>
        <input type="mail" name="email" class="email form-control" placeholder="email">
      </div>

      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="password form-control" placeholder="Type your password">
      </div>

      <div class="form-group">
        <label for="password">Verify your password</label>
        <input type="password" name="password2" class="password2 form-control" placeholder="Re type your password">
      </div>

      <div class="form-group">
        <label for="birth_date">Birth Date</label>
        <input type="date" name="birth_date" class="birth_date form-control" placeholder="Birth date">
      </div>

      <div class="form-group logIn__form--submit">
        <button type="submit" name="submit_inscription" class="btn btn-lg btn-primary" onclick="return signUp()">Connexion</button>
      </div>
    </form>
  </div>
</div>
