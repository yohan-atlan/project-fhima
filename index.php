<?php

// Config
include 'config/options.php';
include 'config/database.php'; // Uncomment if you need database

// Get the query
$q = empty($_GET['q']) ? '' : $_GET['q'];

// Routes
if($q == '')
	$page = 'home';
else if($q == 'about')
	$page = 'about';
else if($q == 'mentions-legales')
		$page = 'mentions-legales';
else if($q == 'contact')
		$page = 'contact';
else if($q == 'claim/new')
		$page = 'new';
else if($q == 'claim/itenary')
		$page = 'itenary';
else if($q == 'claim/fly-number')
		$page = 'fly-number';
else if($q == 'logIn')
		$page = 'logIn';
else if($q == 'profile')
		$page = 'profile';
else if($q == 'signUp')
		$page = 'signUp';
	else if($q == 'myClaim')
		$page = 'myClaim';
else
	$page = '404';

// Includes
include 'controllers/'.$page.'.php';
include 'views/partials/header.php';
include 'views/pages/'.$page.'.php';
include 'views/partials/footer.php';
