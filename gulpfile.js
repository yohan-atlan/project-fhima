var gulp = require('gulp');
var sass = require('gulp-sass');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');
var browserSync = require('browser-sync');
var plumberErrorHandler = { errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};

gulp.task('sass', function () {
  gulp.src('src/css/*.scss')
    .pipe(plumber(plumberErrorHandler))
    .pipe(sass())
    .pipe(gulp.dest('dist/css'))
    .pipe(livereload());
});
gulp.task('js', function () {
    gulp.src('src/js/*/*.js')
        .pipe(jshint())
        .pipe(plumber(plumberErrorHandler))
        .pipe(jshint.reporter('fail'))
        .pipe(gulp.dest('dist/js'))
        .pipe(livereload());
});

gulp.task('img', function() {
  gulp.src('src/images/**/*')
    .pipe(plumber(plumberErrorHandler))
    // .pipe(imagemin({
      // optimizationLevel: 7,
      // progressive: true
    // }))
    .pipe(gulp.dest('dist/img'))
    .pipe(livereload());
});

gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('src/css/*/*.scss', ['sass']);
  gulp.watch('src/js/app/*.js', ['js']);
  gulp.watch('src/images/*/*.{png,jpg,gif}', ['img']);
});


gulp.task('browser-sync', function() {
    browserSync.init(['css/*.css', 'js/*.js'], {
        /*
        I like to use a vhost, WAMP guide: https://www.kristengrote.com/blog/articles/how-to-set-up-virtual-hosts-using-wamp, XAMP guide: http://sawmac.com/xampp/virtualhosts/
        */
        proxy: 'http://localhost:8888/project-fhima/'
        // proxy: 'http://localhost:8888/projet_perso/project-fhima/'
        /* For a static server you would use this: */
        /*
        server: {
            baseDir: './'
        }
        */
    });
});

gulp.task('default', ['sass', 'js', 'img', 'watch', 'browser-sync']);
